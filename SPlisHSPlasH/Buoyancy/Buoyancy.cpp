
#include "Buoyancy.h"
#include "../Simulation.h"
#include "../TimeManager.h"
#include "../BoundaryModel_Akinci2012.h"

using namespace GenParam;
using namespace SPH;

int Buoyancy::ANGULAR_VISCOCITY = -1;

Buoyancy::Buoyancy(FluidModel *model) :
	BuoyancyBase(model)
{
	m_normal.resize(model->numParticles(), Vector3r::Zero());
	m_omega.resize(model->numParticles(), Vector3r::Zero());
	m_gradOmega.resize(model->numParticles(), Vector3r::Zero());
	m_gradT.resize(model->numParticles(), Vector3r::Zero());
	
	m_viscosity = static_cast<Real>(0.01);
}

Buoyancy::~Buoyancy()
{
	m_normal.clear();
	m_omega.clear();
	m_gradOmega.clear();
	m_gradT.clear();
}

void Buoyancy::initParameters()
{
	BuoyancyBase::initParameters();
	
	ANGULAR_VISCOCITY =
		createNumericParameter("viscocity", "angular viscosity", &m_viscosity);
	setGroup(ANGULAR_VISCOCITY, "Buoyancy force");
	setDescription(ANGULAR_VISCOCITY,
					"viscosity for angular velocity");
	auto rparam = static_cast<RealParameter *>(getParameter(ANGULAR_VISCOCITY));
}
void Buoyancy::step()
{
	Simulation *sim = Simulation::getCurrent();
	sim->getNeighborhoodSearch()->find_neighbors();

	const unsigned int numParticles = m_model->numActiveParticles();
	const unsigned int fluidModelIndex = m_model->getPointSetIndex();
	auto& model = m_model;

	const Real density_0 = m_model->getDensity0();
	const Vector3r gravity { 0.0, 9.81, 0.0};

	const Real h = sim->getSupportRadius();
	const Real h2 = h*h;
	
	// angular velocity
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)numParticles; i++)
		{
			const Vector3r &xi = m_model->getPosition(i);
			const Vector3r &vi = m_model->getVelocity(i);
			const Real density_i = m_model->getDensity(i);
			const Real temperature_i = m_model->getTemperature(i);
			Vector3r &omegai = m_omega[i];
			Vector3r &gradTi = m_gradT[i];
			Vector3r omegai_dampening;
			omegai_dampening.setZero();
			//omegai.setZero();
			Vector3r &gradOmegai = m_gradOmega[i];
			gradOmegai.setZero();

			Vector3r& normal = m_normal[i];
			normal.setZero();
			Matrix3r gradV;
			gradV.setZero();

			// Calculate the vorticity
			forall_fluid_neighbors_in_same_phase(
				const Vector3r &vj = m_model->getVelocity(neighborIndex);
				const Real density_j = m_model->getDensity(neighborIndex);
				const Real temperature_j = m_model->getTemperature(neighborIndex);
				const Real mass = m_model->getMass(neighborIndex);
				const Vector3r omegaj =  m_omega[neighborIndex];

				const Vector3r xDiff = (xi - xj);
				const Vector3r vDiff = (vj - vi);
				
				const Vector3r gradW = sim->gradW(xDiff);
				const Real volume = (1.0 / density_j) * mass;

				//difference formula
				gradV += volume * vDiff * gradW.transpose();
				normal += volume * gradW;
				omegai_dampening += (mass / density_j) * (omegai - omegaj).dot(xDiff) / (xDiff.squaredNorm() + 0.01*h2) * gradW;
				gradTi += volume * (temperature_j - temperature_i) * gradW;
			);

			gradOmegai = gradV * omegai + m_vorticity * normal.cross(gravity) + m_viscosity * omegai_dampening;
			omegai += TimeManager::getCurrent()->getTimeStepSize() * gradOmegai;
		}

	}

	const unsigned int nFluids = sim->numberOfFluidModels();
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)numParticles; i++)
		{
			Vector3r &ai = m_model->getAcceleration(i);
			const Vector3r &vi = m_model->getVelocity(i);
			const Vector3r &xi = m_model->getPosition(i);
			const Real density_i = m_model->getDensity(i);
			Real& temperature = m_model->getTemperature(i);
			Real temperature0 = m_model->getTemperatureAmbient();

			// Upward force if the particle temperature is higher than the ambient temperature
			auto density_relation = static_cast<Real>(1.0) - density_i / density_0;

			auto t_diff = temperature - temperature0;
			auto deltaT = -(m_temperature_cooling * t_diff * density_relation);
			temperature += deltaT * TimeManager::getCurrent()->getTimeStepSize();

			
			t_diff = temperature - temperature0;
			// This buoyancy model only modifies the force along the Y-axis. 
			ai[1] += m_buoyancy_alpha * t_diff - m_buoyancy_beta * density_relation;
			

			//Determine the acceleration caused by the vorticity
			Vector3r vort;
			vort.setZero();
			forall_fluid_neighbors_in_same_phase(
				vort += m_omega[neighborIndex].cross(xi - xj)*sim->W(xi - xj);
			);

			ai += vort/ density_i;

		}
	}
}

void Buoyancy::reset()
{

	for (unsigned int i = 0; i < m_omega.size(); i++)
	{
		m_omega[i].setZero();
		m_gradOmega[i].setZero();
		m_normal[i].setZero();
		m_gradT[i].setZero();
	}
}

void Buoyancy::performNeighborhoodSearchSort()
{
	const unsigned int numPart = m_model->numActiveParticles();
	if (numPart == 0)
		return;

	Simulation *sim = Simulation::getCurrent();
	auto const& d = sim->getNeighborhoodSearch()->point_set(m_model->getPointSetIndex());
	d.sort_field(&m_omega[0]);
	d.sort_field(&m_gradOmega[0]);
	d.sort_field(&m_normal[0]);
	d.sort_field(&m_gradT[0]);
}