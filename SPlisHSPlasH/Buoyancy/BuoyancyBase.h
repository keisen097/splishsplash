#ifndef __BuoyancyBase_h__
#define __BuoyancyBase_h__

#include "SPlisHSPlasH/Common.h"
#include "SPlisHSPlasH/FluidModel.h"
#include "SPlisHSPlasH/NonPressureForceBase.h"

namespace SPH {
/** \brief Base class for all elasticity methods.
 */
class BuoyancyBase : public NonPressureForceBase
{
protected:

	Real m_buoyancy_alpha;
	Real m_buoyancy_beta;
	Real m_temperature_cooling;
	Real m_vorticity;

	virtual void initParameters();

public:
	static int BUOYANCY_COEFFICIENT_ALPHA;
	static int BUOYANCY_COEFFICIENT_BETA;
	static int VORTICITY_COEFFICIENT;
	static int TEMPERATURE_COOLING_COEFFICIENT;

	BuoyancyBase(FluidModel *model);
	virtual ~BuoyancyBase(void);

	virtual void performNeighborhoodSearchSort() {};
};
}
#endif