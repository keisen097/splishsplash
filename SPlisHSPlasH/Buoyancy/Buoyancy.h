#ifndef __Buoyancy_h__
#define __Buoyancy_h__

#include "BuoyancyBase.h"

namespace SPH {

class Buoyancy : public BuoyancyBase
{
/**
 * @brief This class implements the buoyancy forces and turbulence 
 *  References: 
 * 	The Baroclinic Turbulence by Macklin et al. [MMCK14] URL: https://mmacklin.com/uppfrta_preprint.pdf
 * 	Temperature and buoyancy model is based on Fedkiw et al. [FSJ01] URL: https://web.stanford.edu/class/cs237d/smoke.pdf
 */
private:
	std::vector<Vector3r> m_omega;
	std::vector<Vector3r> m_gradOmega;
	std::vector<Vector3r> m_gradT;
	std::vector<Vector3r> m_normal;

	Real m_viscosity;

	static int ANGULAR_VISCOCITY;
public:
	Buoyancy(FluidModel* model);
	virtual ~Buoyancy(void);

	virtual void initParameters();
	virtual void performNeighborhoodSearchSort();
	
	virtual void step();
	virtual void reset();

	static NonPressureForceBase* creator(FluidModel* model) { return new Buoyancy(model); }
};

}

#endif
