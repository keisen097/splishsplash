#include "BuoyancyBase.h"
#include "../Simulation.h"
#include "../TimeManager.h"

#include "SPlisHSPlasH/Common.h"
#include "SPlisHSPlasH/FluidModel.h"
#include "SPlisHSPlasH/NonPressureForceBase.h"

using namespace SPH;
using namespace GenParam;

int BuoyancyBase::BUOYANCY_COEFFICIENT_ALPHA = -1;
int BuoyancyBase::BUOYANCY_COEFFICIENT_BETA = -1;
int BuoyancyBase::TEMPERATURE_COOLING_COEFFICIENT = -1;
int BuoyancyBase::VORTICITY_COEFFICIENT = -1;

BuoyancyBase::BuoyancyBase(FluidModel *model) :
	NonPressureForceBase(model)
{
	//m_buoyancy = static_cast<Real>(1.0);
	m_temperature_cooling = static_cast<Real>(1.0);
	m_buoyancy_alpha = static_cast<Real>(0.2);
	m_buoyancy_beta = static_cast<Real>(10.0);
	m_vorticity = static_cast<Real>(10.0);
}

BuoyancyBase::~BuoyancyBase(){}

void BuoyancyBase::initParameters()
{
	NonPressureForceBase::initParameters();

	TEMPERATURE_COOLING_COEFFICIENT =
		createNumericParameter("cooling", "Particle cooling coefficient", &m_temperature_cooling);
	setGroup(TEMPERATURE_COOLING_COEFFICIENT, "Buoyancy force");
	setDescription(TEMPERATURE_COOLING_COEFFICIENT,
					"Determines how fast the particles cool down");
	RealParameter *rparam =
		static_cast<RealParameter *>(getParameter(TEMPERATURE_COOLING_COEFFICIENT));
	rparam->setMinValue(0.0);

	VORTICITY_COEFFICIENT =
		createNumericParameter("vorticity", "Particle vorticity", &m_vorticity);
	setGroup(VORTICITY_COEFFICIENT, "Buoyancy force");
	setDescription(VORTICITY_COEFFICIENT,
					"Method to compute the vorticity");
	rparam = static_cast<RealParameter *>(getParameter(VORTICITY_COEFFICIENT));
	
	BUOYANCY_COEFFICIENT_ALPHA =
		createNumericParameter("alpha", "Buoyancy temperature relation", &m_buoyancy_alpha);
	setGroup(BUOYANCY_COEFFICIENT_ALPHA, "Buoyancy force");
	setDescription(BUOYANCY_COEFFICIENT_ALPHA,
					"Determines how impactful the temperature difference is for the buoyancy force");
	rparam = static_cast<RealParameter *>(getParameter(BUOYANCY_COEFFICIENT_ALPHA));
	
	BUOYANCY_COEFFICIENT_BETA =
		createNumericParameter("beta", "Buoyancy density relation", &m_buoyancy_beta);
	setGroup(BUOYANCY_COEFFICIENT_BETA, "Buoyancy force");
	setDescription(BUOYANCY_COEFFICIENT_BETA,
					"Determines how impactful the density is for the buoyancy force");
	rparam = static_cast<RealParameter *>(getParameter(BUOYANCY_COEFFICIENT_BETA));

}